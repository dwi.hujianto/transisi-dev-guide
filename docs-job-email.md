# Pengiriman email dengan menggunakan jobs

Untuk pengiriman email apabila dalam satu waktu mengirim lebih dari 1 email sebaiknya dipisah menjadi 2 jobs

- Job pertama untuk mentrigger kirim ke list email yang telah di query
- Job ke 2 adalah job untuk mengirim email tersebut.

### Contoh job kirim email dibawah kirim emailnya masih jadi satu jobs
```php
public function handle() 
{
    $users = DocumentSigner::select('signer_user_id')
        ->whereBetween('updated_at', [now()->subDay(7), now()])
        ->WhereHas('document', function($q){
            $q->where('status', '!=', StatusDokumen::DRAFT);
        })
        ->with(['user'])
        ->distinct()
        ->get();
    foreach($users as $key => $user)
    {
        if (isset($user['user']->email) && $user['user']->email <> '') {
            Mail::to($user['user']->email)->send(new MailNotify($user['user']->id));
            if (($key + 1) % 4 == 0) {
                sleep(15);
            }
        }
    }
}
```

### Sebaiknya diubah menjadi :

Job1
```php
public function handle() 
{
    $users = DocumentSigner::select('signer_user_id')
        ->whereBetween('updated_at', [now()->subDay(7), now()])
        ->WhereHas('document', function($q){
            $q->where('status', '!=', StatusDokumen::DRAFT);
        })
        ->with(['user'])
        ->distinct()
        ->get();
    foreach($users as $key => $user)
    {
        if (isset($user['user']->email) && $user['user']->email <> '') {
            // setiap kirim email dikirim menggunakan jobs baru
            Job2::dispatch($user)->onQueue('nama-queue-yang-bersangkutan');
        }
    }
}
```
Job2
```php
public function handle()
{
    Mail::to($user['user']->email)->send(new MailNotify($user['user']->id));
    // tidak perlu ada sleep karena tiap email sudah masuk dalam antrian
    //if (($key + 1) % 4 == 0) {
    //    sleep(15);
    //}
}

```
### Meminimalisir duplikasi job dan mengurangi request ke provider yang terlalu tinggi

Batasi max attempts jobs pengiriman email hanya 1 kali, contoh:

```php
class SendEmailDailyMess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $tries = 1;

    // code...
```

Tambahkan delay untuk execute jobs antara 3 sampai 5 seconds untuk menghindari concurrent request yang terlalu sering ke provider email, contoh:

```php
SendEmailLogDepartmentJob::dispatch(
    $value->department_head_email,
    $ccMail,
    convert_date_dayname_id($hitDate),
    convert_date_dayname_id($date->format('Y-m-d')),
    $data
)->onQueue('presensi')->delay(now()->addSeconds(3));

```

