# Transisi Dev Guide

**Step 1**
 
Pelajari dan setup laravel 6 atau 7 + module nwidart (https://github.com/nwidart/laravel-modules) laravel modules

**Step 2**

Jika sudah selesai setup dan tidak ada error pada step 1, selanjutnya adalah instalasi RBAC menggunakan spati
e permission (https://spatie.be/docs/laravel-permission/v4/introduction)

**Step 3** 
Buatlah module baru menggunakan nwidart(misal nama module-nya Transisi) yang didalamnya terdapat CRUD (companies & employees boleh menggunakan Test Laravel Dev) standard migration table
* Penamaan table : usahakan menggunakan pluralization, misal company jadi companies
* Penamaan field : simple dan informatif, tidak perlu ada prefix/subfix, misal:
     - **Bad** : (`company_id, company_name, company_created_at, company_updated_at, company_deleted_at`) 
     - **Good** (`id, name, created_at, updated_at, deleted_at`)
* Usahan kode sesuai standarisasi psr (https://www.php-fig.org/psr/psr-1/)
* Untuk fungsi/logic code yang kompleks pisah class sendiri agar code tidak terlalu gemuk dan menghindari spaghetti code
* Gunakan simple Repository pattern untuk proses dan query data:
```php
<?php
namespace Modules\Transisi\Repositories;

use Modules\Transisi\Repositories\Entities\Company;

class CompanyRepository
{
    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model->find($id)
    }

    public function fetch(array $params)
    {
        $query = $this->model->query();

        if (isset($params['status'])) {
            $query->where('status', $params['status']);
        }

        return $query = $query->paginate();
    }

    // method lainya ...
}
```
* Tambahkan satu field `status int` pada table employees serta class constant dengan php didalam folder `[project-name]/Modules/Transisi/Constants/Status.php`, dimana nantinya isi field status akan sama dengan list constant yang ada di class Status.php tersebut 
```php
<?php
namespace Modules\Transisi\Constants;

class Status
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const BLOCK = 3;

    public static function labels(): array
    {
        return [
            self::ACTIVE => "Aktif",
            self::INACTIVE => "Tidak Aktif",
            self::BLOCK => "Di Block",
        ];
    }
}
```


**Step 4** 

Buat simple report list data emplyee per company dengan menggunakan laravel dompdf (https://github.com/barryvdh/laravel-dompdf)

**Step 5**

Big data (dashboard, statistik, export, pdf) wajib menggunakan select dan raw query tidak menggunakan eloquent

**Step 6** 

Bulk insert menggunakan method insert bukan loop method create (misal: 
import excel data employees)

**Step 7** 

Select2 menggunakan paginasi ajax bukan loop di html (berat ketika load row data)

**Step 8** 

Untuk fungsi yg kompleks pembuatan class sendiri

**Step 9** 

Repository pattern digunakan untuk library 3rd party yg kemungkinan bisa berubah2 (misal sms gateway) untuk query tidak perlu

**Step 10** 

Git workflow
*  Checkout master atau jika sudah dibranch master, lakukan rebase untuk code yang terbaru `git pull --rebase origin master`
*  Jika pekerjaan yang dilakukan adalah membuat fitur baru, maka bikin branch baru misal `dev-sliders` (pengerjaan dilakukan di branch ini)
*  Apabila pekerjaanya sudah selesai/sudah di commit sebelum push ke repo WAJIB `git pull --rebase origin master` dari posisi branch aktif `dev-sliders` baru setelah itu boleh dipush ke repo git push origin `dev-sliders`
