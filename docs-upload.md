# Upload file

Upload file sebisa mungkin menggunakan package yang sudah disediakan laravel https://laravel.com/docs/6.x/filesystem, supaya driver bisa diganti sesuai kebutuhan environment-nya. Untuk development usahakan menggunkan driver local, gunakan s3 jika memang dibutuhkan (debugging) karena s3 akan mengenakan biaya upload/request file, contoh:

```php
// file: .env
// Production env
FILESYSTEM=s3
// Development env
FILESYSTEM=local

Storage::disk(env('FILESYSTEM'))->put('manpower/mine-permit/manpower.pdf');
```

### Struktur folder

Standard struktur folder untuk upload file adalah `{nama-module}/{nama-feature}/{nama-file}`, contoh:
```php
Storage::disk(env('FILESYSTEM'))->put('manpower/mine-permit/manpower.pdf');
```
Usahakan menyimpan info file dengan **relative path** pada database, hindari menyimpan path secara **absolute path** supaya lebih dinamis, contoh:

Relative path
```php
manpower/mine-permit/manpower.pdf
```

Absolute path
```php
https://s3-bucket.transisi.space/manpower/mine-permit/manpower.pdf
```
